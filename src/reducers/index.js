import { combineReducers } from "redux";
import messages from "../messages/reducer";
import updateForm from "../updateMessage/reducer";
import login from "../loginPage/reducer";

const rootReducer = combineReducers({
    messages,
    updateForm,
    login
    
});

export default rootReducer;
import { all } from 'redux-saga/effects';
// import userPageSagas from '../userPage/sagas';
import messagesSagas from '../messages/sagas';
import loginSagas from '../loginPage/sagas';
import messageEditorSagas from '../updateMessage/sagas';

export default function* rootSaga() {
    yield all([
        messageEditorSagas(),
        messagesSagas(),
        loginSagas()
    ])
};
import React from 'react';
import MessageList from './messages/index';
import SignIn from './loginPage';
import MessageEditor from './updateMessage';
// import UserPage from './userPage/index';
import { Switch, Route } from 'react-router-dom';
import './App.css';

function App() {
	return (
		<div className="App">
			<Switch>
				<Route exact path='/' component={SignIn} />
				<Route exact path='/messages' component={MessageList} />
				<Route path='/message/:id' component={MessageEditor} />
				{/* <Route exact path="/user" component={UserPage} />
				<Route path="/user/:id" component={UserPage} /> */}
			</Switch>
		</div>
	);
}

export default App;
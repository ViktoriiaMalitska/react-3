import {
  FETCH_MESSAGES_REQUEST,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESAAGES_FAILURE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  ADD_MESSAGE,
  LIKE_MESSAGE
} from "./actionTypes";



export default function (state = [], action) {
  switch (action.type) {
      case FETCH_MESSAGES_SUCCESS: {
          return [...action.payload.messages];
      }
      default:
          return state;
  }

  
}
import React, { Component } from "react";
import { connect } from "react-redux";
import Header from "../components/Header/Header";
import OutHeader from "../components/OutHeader";
import NewMessageForm from "../components/createMessage";
import Footer from "../components/Footer";
import Message from "./Message";
import UpdatePage from '../updateMessage';
import * as actions from "./actions";
import { USER_ID } from "../config";

class MessageList extends Component {
  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onLike = this.onLike.bind(this);
  }

  onEdit(id) {
      this.props.history.push(`/message/${id}`);
  }

  onUpdate(id, data) {
    this.props.updateMessage(id, data);
  }

  onDelete(id) {
    this.props.deleteMessage(id);
  }

  onAdd() {
    this.props.addMessage();
  }

  onLike(id) {
    this.props.likeMessage(id);
  }

  componentDidMount() {
    this.props.fetchMessages();
  }

  render() {
    if (this.props.error) {
      return <div>Error! {this.props.error.message}</div>;
    }

    if (this.props.loading) {
      return <div>Loading...</div>;
    }
    return (
      <div className="mainContainer">
        <OutHeader />
        <Header />
        {/* <UpdatePage/> */}
        <div className="messagesContainer">
        {this.props.messages.map((message) => {
          return (
            <Message
              key={message.id}
              id={message.id}
              userId={message.id}
              text={message.text}
              avatar={message.userId === USER_ID ? "#" : message.avatar}
              date={message.createdAt}
              onEdit={this.onEdit}
              onUpdate={this.onUpdate}
              onDelete={this.onDelete}
              onLike={this.onLike}
            />
          );
        })}
        </div>
        <NewMessageForm />
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);

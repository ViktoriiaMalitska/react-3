import {ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES_REQUEST } from "./actionTypes";
import service from "./service";



export const fetchMessages = () => ({
    type: FETCH_MESSAGES_REQUEST
});



export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
      id,
      data,
    },
  });

  export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
  });

  export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewId(),
        data
    }
});

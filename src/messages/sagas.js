import axios from 'axios';
import { API } from '../config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_MESSAGES_REQUEST, FETCH_MESSAGES_SUCCESS, UPDATE_MESSAGE, DELETE_MESSAGE, ADD_MESSAGE } from "./actionTypes";

export function* fetchMessages() {
	try {
        const messages = yield call(axios.get, `${API}/messages`);
		yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data } })
	} catch (error) {
		console.log('fetchMessages error:', error.message)
	}
}

function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES_REQUEST, fetchMessages)
}


export function* updateMessage(action) {
	const id = action.payload.id;
	const updatedMessage = { ...action.payload.data };
	
	try {
		yield call(axios.put, `${API}/messages/${id}`, updatedMessage);
		yield put({ type: FETCH_MESSAGES_REQUEST });
	} catch (error) {
		console.log('updateMessages error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${API}/messages/${action.payload.id}`);
		yield put({ type: FETCH_MESSAGES_REQUEST })
	} catch (error) {
		console.log('deleteMessage Error:', error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}


export function* addMessage(action) {
    const newMessage = { ...action.payload.data, id: action.payload.id };
	try {
		yield call(axios.post, `${API}/messages`, newMessage);
		yield put({ type: FETCH_MESSAGES_REQUEST });
	} catch (error) {
		console.log('createMessage error:', error.message);
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export default function* messagesSagas() {
	yield all([
		watchFetchMessages(),
		watchAddMessage(),
		watchUpdateMessage(),
        watchDeleteMessage()
	])
};
import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./actions";
import { updateMessage } from "../messages/actions";
import TextInput from "./TextInput";


class MessageEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    // this.onCancel = this.onCancel.bind(this);
    // this.onSave = this.onSave.bind(this);
    // this.onChangeData = this.onChangeData.bind(this);
}

componentDidMount() {
    if (this.props.match.params.id) {
        this.props.fetchMessage(this.props.match.params.id);
        console.log(this.props)
    }
}

// static getDerivedStateFromProps(nextProps, prevState) {
//     if (nextProps.userData.id !== prevState.id && nextProps.match.params.id) {
//         return {
//             ...nextProps.userData
//         };
//     } else {
//         return null;
//     }
// }

onCancel() {
    // this.setState(this.getDefaultUserData());
    this.props.history.push('/messages');
}

onSave() {
    
        this.props.updateMessage(this.state.id, this.state);
    
    // this.setState(this.getDefaultUserData());
    this.props.history.push('/');
}

onChangeText(e) {
    const value = e.target.value;
    this.setState(
        {
            
        }
    );
}

// getDefaultUserData() {
//     return {
//         ...defaultUserConfig
//     };
// }

// getInput(data, { label, type, keyword }, index) {
//     switch (type) {
//         case 'text':
//             return (
//                 <TextInput
//                     key={index}
//                     label={label}
//                     type={type}
//                     text={data[keyword]}
//                     keyword={keyword}
//                     onChange={this.onChangeText}
//                 />
//             );
//         default:
//             return null;
//     }
// }

render() {
    const data = this.state;
  console.log(this.props)
    return (
        <div className="modal" style={{ display: "block" }} tabIndex="-1" role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content" style={{ padding: "5px" }}>
                    <div className="modal-header">
                        <h5 className="modal-title">Add user</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onCancel}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div className="modal-footer">
                        <button className="btn btn-secondary" onClick={this.onCancel}>Cancel</button>
                        <button className="btn btn-primary" onClick={this.onSave}>Save</button>
                    </div>
                </div>
            </div>
        </div>
    );
}
}

  


const mapStateToProps = (state) => {
  return {
    message: state.messages
  };
};

const mapDispatchToProps = {
  ...actions,
  updateMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);

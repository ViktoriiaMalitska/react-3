import { FETCH_MESSAGE_SUCCESS } from "./actionTypes";

const initialState = {
    message: {

    }
};

export default function (state = initialState, action) {
    
    switch (action.type) {
        case  FETCH_MESSAGE_SUCCESS:{
            console.log(action.payload)
        return action.payload.message;
    }
        default:
            return state;
    }
}

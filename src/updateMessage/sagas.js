import axios from 'axios';
import { API } from '../config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS } from './actionTypes';

export function* getMessage(action) {
    const id = action.payload.id;
    console.log(id)
	try {
        const message = yield call(axios.get, `${API}/messages/${id}`);
        yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { message: message.data }  })
		
	} catch (error) {
		console.log('updateMessages error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(FETCH_MESSAGE_REQUEST, getMessage)
}

export default function* messageEditorSagas() {
	yield all([
		watchUpdateMessage()
	])
};
import React, { Component } from "react";
import { connect } from "react-redux";
import { CHAT_NAME } from "../../config";

class Header extends Component {
  getTotalUsers = (list) => {
    let setId = new Set();
    list.forEach((element) => {
      setId.add(element.userId);
    });
    return setId.size;
  };
  getlastDate = (list) => {
    let date;
    let sortListMessage = list.sort((cur, next) => {
      return new Date(cur.createdAt) - new Date(next.createdAt);
    });

    let lastMessage = list[sortListMessage.length - 1];
    if (!!lastMessage) {
      let lastData = lastMessage.createdAt;
      date = new Date(Date.parse(lastData));

      date = `${date.getHours() - 3}:${date.getMinutes()}`;
    } else lastMessage = null;
    return date;
  };

  render() {
   
    let listMessage = this.props.messages;
    let totalUsers = this.getTotalUsers(listMessage);
    let totalMessages = listMessage.length;
    let lastData = this.getlastDate(listMessage);
    return (
      <div className="headerContainer">
        <div className="headeritem">{CHAT_NAME}</div>
        <div className="headeritem">{`${totalUsers}  participants`}</div>
        <div className="headeritem">{`${totalMessages}  messages`}</div>
        <div className="headeritem">{`last message at ${lastData}`}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
  };
};

export default connect(mapStateToProps)(Header);

import axios from 'axios';
import { API } from '../config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGIN_USER_SUCCESS  } from './actionTypes';

export function* loginUser(action) {
	const user = { ...action.payload.data};
	console.log(user)
	try {
        let res = yield call(axios.post, `${API}/login`, user);
        console.log(action)
        yield put({ type: LOGIN_USER_SUCCESS, payload: { user: res.data } } ) 
	} catch (error) {
		console.log('login error', error.message);
	}
}

function* watchloginUser() {
	yield takeEvery(LOGIN_USER, loginUser)
}


export default function* loginSagas() {
	yield all([
		watchloginUser()
	])
};
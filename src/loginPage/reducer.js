
import { LOGIN_USER_SUCCESS } from "./actionTypes";


export default function (state = [], action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS: {
            state.path = `/messages`
            return state
        }
        default:
            return state;
    }
  
    
  }
import React, { Component, useState } from "react";

import { connect } from "react-redux";
import * as actions from "./actions";

function Login({onSubmit}) {
  const [email, setEmail] = useState();
  const [password, setPas] = useState();
  const onPasChange = (event) => {
    setPas(event.target.value);
  };
  const onEmailChange = (event) => {
    setEmail(event.target.value);
  };
  
  return (
    <div>
      <input
        type="text"
        placeholder="Enter Email"
        value={email}
        onChange={onEmailChange}
        type="email"
      />
      <input
        type="text"
        placeholder="Enter Password"
        value={password}
        onChange={onPasChange}
        type="pas"
      />
      <button onClick={()=>onSubmit({email, password})}>Login</button>
    </div>
  );
}
class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.onLogin = this.onLogin.bind(this);
  }
  
  
  onLogin(data) {
    this.props.loginUser(data);
    this.props.redirect()
  }

  render() {
    return <Login onSubmit={this.onLogin}/>;
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.login,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

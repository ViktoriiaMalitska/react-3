import { LOGIN_USER, LOGIN_USER_SUCCESS } from "./actionTypes";


export const loginUser = (data) => ({
    type: LOGIN_USER,
    payload: {
      data
    },
  });



  export const redirect = () => ({
    type: LOGIN_USER_SUCCESS
  });
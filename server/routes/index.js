const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');
const messageRoutes = require('./messageRoutes');

module.exports = (app) => {
    app.use('/users', userRoutes);
    app.use('/messages', messageRoutes);
    app.use('/login', authRoutes);
  };
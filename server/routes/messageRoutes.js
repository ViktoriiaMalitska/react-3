const { Router } = require("express");
const MessageService = require("../services/messageService");
// const { createUserValid, updateUserValid } = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post('/',  (req, res, next) => {
    try {
      const data = MessageService.createMessage(req);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get('/', (req, res, next) => {
    try {
      const data = MessageService.searchAll();
      res.data = data;
      console.log("from ")
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put('/:id', (req, res, next) => {
    try {
      const data = MessageService.updateOne(req);
      // console.log(data)
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get('/:id', (req, res, next) => {
    try {
      const data = MessageService.search(req);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete('/:id', (req, res, next) => {
    try {
      const data = MessageService.deleteOne(req);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
// TODO: Implement route controllers for user

module.exports = router;

const { MessageRepository } = require("../repositories/messageRepository");

class MessageService {
  
  createMessage(date) {
    const newMessage = date.body;
    const message = {
      avatar: newMessage.avatar,
      userId: newMessage.userId,
      user:newMessage.user,
      text: newMessage.text
    };

    const item = MessageRepository.create(message);
    if (!item) {
      return null;
    }
    return item;
  }

  search(search) {
    const id = search.params;
    const item = MessageRepository.getOne(id);
    if (!item) {
      return null;
    }
    return item;
  }

  searchAll() {
    const items = MessageRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  updateOne(req) {
    const id = req.params.id;
    const data = req.body;
    const items = MessageRepository.update(id, data);
    if (!items) {
      return null;
    }
    return items;
  }

  deleteOne(data) {
    const id = data.params.id;
    const items = MessageRepository.delete(id);
    if (items.length === 0) {
      return null;
    }
    return items[0];
  }
}

module.exports = new MessageService();
